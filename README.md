## ESP32 powermeter
- can meassure voltage and currency
- is wired between consumer and eg battery
- USB type C connectors

### Parts list
- Adafruit ESP32-S2 Reverse TFT Feather (SPI ST7789)
- Adafruit INA219 High Side DC Current Sensor Breakout (I2C)
- Slider switch for power off (reset button already on s2 board)
- USB type C cables with VCC and GND connection and JST connectors

### HOWTO flash with esptool.py

Note for myself<br>
`espflash save-image --merge --chip esp32s2 target/xtensa-esp32s2-none-elf/release/esp32s2-powermeter firmware.bin`

`esptool.py --chip esp32s2 --port /dev/ttyACM1 write_flash -z 0x0 firmware.bin`